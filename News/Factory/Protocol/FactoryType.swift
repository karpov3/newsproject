//
//  FactoryType.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import Foundation
import NewsViewModel
import NewsService

protocol FactoryType {

    func listNewsViewModel(didSelect: @escaping (News) -> Void) -> ListNewsViewModeling

    func listNewsObservableViewModel() -> ListNewsObservableViewModel

    func detailNewsViewModel(identifier: Int, defaultNews: News?) -> DetailNewsViewModeling

    func detailNewsObservableViewModel(identifier: Int, defaultNews: News?) -> DetailNewsObservableViewModel
}
