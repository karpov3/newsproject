//
//  Factory.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import Foundation
import NewsService
import NewsViewModel
import Moya

class Factory: FactoryType {

    func listNewsObservableViewModel() -> ListNewsObservableViewModel {
        ListNewsObservableViewModel(service: service)
    }

    func detailNewsObservableViewModel(identifier: Int, defaultNews: News?) -> DetailNewsObservableViewModel {
        DetailNewsObservableViewModel(service: service, identifier: identifier, defaultNews: defaultNews)
    }

    // MARK: FactoryType
    func listNewsViewModel(didSelect: @escaping (News) -> Void) -> ListNewsViewModeling {
        NewsListViewModel(service: service, didSelect: didSelect)
    }

    func detailNewsViewModel(identifier: Int, defaultNews: News?) -> DetailNewsViewModeling {
        NewsDetailViewModel(service: service, identifier: identifier, defaultNews: defaultNews)
    }

    // MARK: Services

    lazy var service: NewsServiceType = createService(config: config)

    internal func createService(config: ServiceConfiguration) -> NewsServiceType {
        let authenticator = ServiceAuthenticator(apiKey: config.apiKey)
        let provider = MoyaProvider<NewsService>(serviceAuthenticator: authenticator)
        return NewsClient(baseURL: config.baseURL, provider: provider)
    }

    // MARK: Initialization
    let config: ServiceConfiguration

    init?() {

        guard let filePath = Bundle.main.path(forResource: "configuration", ofType: "json") else {
            print("Missing configuration.json file")
            return nil
        }

        let fileUrl = URL(fileURLWithPath: filePath)

        do {
            let data = try Data(contentsOf: fileUrl)
            self.config = try JSONDecoder().decode(ServiceConfiguration.self, from: data)
        } catch {
            print("Bad configuration of configuration.json file")
            return nil
        }
    }
}
