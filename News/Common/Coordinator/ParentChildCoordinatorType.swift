//
//  ParentChildCoordinatorType.swift
//  News
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation

public protocol ParentChildCoordinatorType: AnyObject {

    var childCoordinators: [ParentChildCoordinatorType] { get set }

    var parentCoordinator: ParentChildCoordinatorType? { get set }
}

public extension ParentChildCoordinatorType {

    func add(childCoordinator: ParentChildCoordinatorType) {
        childCoordinators.append(childCoordinator)
        childCoordinator.parentCoordinator = self
    }

    func remove(childCoordinator: ParentChildCoordinatorType) {
        if let index = childCoordinators.firstIndex(where: { $0 === childCoordinator }) {
            childCoordinators.remove(at: index)
            childCoordinator.parentCoordinator = nil
        }
    }

    func removeFromParentCoordinator() {
        parentCoordinator?.remove(childCoordinator: self)
    }
}
