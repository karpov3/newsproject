//
//  NavigationCoordinatorType.swift
//  News
//
//  Created by Alexander Karpov on 22/09/22.
//

import UIKit

public protocol NavigationCoordinatorType: CoordinatorType {

    /// Navigation controller as root of current flow
    var navigationController: UINavigationController? { get set }

}
