//
//  CoordinatoryType.swift
//  News
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation

public protocol CoordinatorType: AnyObject {

    associatedtype DataSourceType

    func start(withDataSource dataSource: DataSourceType)

}
