//
//  DateFormatter+Utility.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import Foundation

extension DateFormatter {
  public static let label: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd MMMM yyyy HH:mm"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    return formatter
  }()
}
