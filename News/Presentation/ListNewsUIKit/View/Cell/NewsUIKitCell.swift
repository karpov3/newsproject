//
//  NewsUIKitCell.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import UIKit
import NewsService

class NewsUIKitCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var date: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setup(with news: News) {
        self.title.text = "#\(news.id). \(news.title)"
        self.subtitle.text = news.subtitle
        self.date.text = DateFormatter.label.string(from: news.timestamp)

    }
}
