//
//  ListNewsUIKitViewController.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import UIKit
import NewsViewModel
import NewsService

class ListNewsUIKitViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var news: [News] = []

    var loadingChunks: Int = 0

    @IBOutlet weak var tableView: UITableView!

    // MARK: Initialization

    var viewModel: ListNewsViewModeling!

    var deallocating: (() -> Void)?

    public init(viewModel: ListNewsViewModeling) {
        super.init(nibName: "ListNewsUIKitViewController", bundle: Bundle(for: type(of: self)))
        self.viewModel = viewModel
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: UIViewController lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        loadData()
    }

    func loadData() {

        loadingChunks += 1

        viewModel.load(completion: {[weak self] result in

            guard let `self` = self else { return }

            do {
                self.news = try result.get()
            } catch {
                var errorMessage = error.localizedDescription
                if let error = error as? NewsError {
                    errorMessage = error.description
                }
                let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                  }))
                self.present(alert, animated: true, completion: nil)
            }
            self.tableView.reloadData()
            self.loadingChunks -= 1

        })
    }

    func setupView() {
        tableView.register(UINib(nibName: "NewsUIKitCell", bundle: nil), forCellReuseIdentifier: "NewsUIKitCellReuseIdentifier")
        tableView.dataSource = self
        tableView.delegate = self
    }

    // MARK: UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count + min(loadingChunks, 1)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row + 1 == news.count {
            loadData()
        }

        if news.count == 0 || news.count == indexPath.row {

            let cell = UITableViewCell()
            cell.accessoryType = .none
            cell.isUserInteractionEnabled = false
            let activityIndicator = UIActivityIndicatorView(style: .medium)
            activityIndicator.startAnimating()
            cell.accessoryView = activityIndicator
            cell.textLabel?.text = "Loading news ..."
            cell.textLabel?.textColor = .lightGray
            return cell
        }

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsUIKitCellReuseIdentifier", for: indexPath) as? NewsUIKitCell else {
            return UITableViewCell()
        }
        cell.accessoryType = .disclosureIndicator
        cell.setup(with: news[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if indexPath.row < news.count {
            viewModel.didSelect(news: news[indexPath.row])
        }
    }

    deinit {
        if let closure = deallocating {
            closure()
        }

    }

}
