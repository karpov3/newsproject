//
//  DetailNewsUIKitViewController.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import UIKit
import NewsViewModel
import NewsService

class DetailNewsUIKitViewController: UIViewController {
    
    // MARK: Properites
    
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Data
    var detail: Detail?
    
    // MARK: Initialization
    
    var viewModel: DetailNewsViewModeling!
    
    public init(viewModel: DetailNewsViewModeling) {
        super.init(nibName: "DetailNewsUIKitViewController", bundle: Bundle(for: type(of: self)))
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateDetail()
        loadData()
    }
    
    func loadData() {
        
        activityIndicator.startAnimating()
        viewModel.load(completion: {[weak self] result in
            
            guard let `self` = self else { return }
            
            do {
                self.detail = try result.get()
                self.updateDetail()
            } catch {
                var errorMessage = error.localizedDescription
                if let error = error as? NewsError {
                    errorMessage = error.description
                }
                let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                  }))
                self.present(alert, animated: true, completion: nil)
            }
            self.activityIndicator.stopAnimating()
            
        })
    }
    
    func updateDetail() {
        
        if let news = detail?.news ?? viewModel.defaultNews {
            header.text = "#\(news.id). \(news.title)"
            subtitle.text = news.subtitle
            date.text = DateFormatter.label.string(from: news.timestamp)
        }
        
        body.text = detail?.body
        
    }
    
}
