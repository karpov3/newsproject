//
//  HomeViewController.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import UIKit

class HomeViewController: UIViewController {

    // MARK: Initialization
    var viewModel: HomeViewModelType!

    public init(viewModel: HomeViewModelType) {
        super.init(nibName: "HomeViewController", bundle: Bundle(for: type(of: self)))
        self.viewModel = viewModel
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Actions

    @IBAction func uikitActionButton(_ sender: UIButton) {
        viewModel.didSelect(.uikit)
    }

    @IBAction func swiftuiActionButton(_ sender: UIButton) {
        viewModel.didSelect(.swiftui)
    }

}
