//
//  HomeViewModel.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import Foundation

class HomeViewModel: HomeViewModelType {
    
    // MARK: HomeViewModelType
    func didSelect(_ kind: HomeActionType) {
        didSelectAction(kind)
    }
    
    // MARK: Initialization
    
    let didSelectAction: (HomeActionType) -> Void
    
    init(didSelectAction: @escaping (HomeActionType) -> Void) {
        self.didSelectAction = didSelectAction
    }
    
}
