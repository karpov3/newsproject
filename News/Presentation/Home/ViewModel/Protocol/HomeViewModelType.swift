//
//  HomeViewModelType.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import Foundation

public enum HomeActionType {
    case uikit, swiftui
}

protocol HomeViewModelType {

    func didSelect(_ kind: HomeActionType)

}
