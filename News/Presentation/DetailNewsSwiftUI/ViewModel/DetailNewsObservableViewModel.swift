//
//  ListNewsObservableViewModel.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import Foundation
import NewsService
import NewsViewModel

class DetailNewsObservableViewModel: NewsDetailViewModel, ObservableObject {
    
    @Published private(set) var detail: Detail?
    @Published private(set) var isLoading: Bool = false
    @Published private(set) var errorMessage: String = ""
    
    func startLoad() {
        self.isLoading = true
        
        self.load {[weak self] result in
            
            guard let `self` = self else { return }
            
            do {
                self.detail = try result.get()
            } catch {
                self.errorMessage = error.localizedDescription
                if let error = error as? NewsError {
                    self.errorMessage = error.description
                }

            }
            self.isLoading = false
        }
        
    }
    
    public override init(service: NewsServiceType, identifier: Int, defaultNews: News?) {
        super.init(service: service, identifier: identifier, defaultNews: defaultNews)
        
    }
}
