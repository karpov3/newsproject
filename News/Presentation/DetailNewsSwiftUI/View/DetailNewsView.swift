//
//  ListNewsUIKitViewController.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import SwiftUI
import NewsService
import NewsViewModel

struct DetailNewsView: View {
    
    var body: some View {
        
        ScrollView {
            VStack(alignment: .leading, spacing: 8, content: {
                
                if let news = stateObject.detail?.news ?? stateObject.defaultNews {
                    Text("#\(news.id.description). \(news.title)")
                        .font(.title)
                    Text(news.subtitle)
                        .font(.headline)
                    
                    if let body = stateObject.detail?.body {
                        Text(body)
                            .font(.body)
                    } else {
                        ProgressView()
                    }
                    
                    Text(DateFormatter.label.string(from: news.timestamp))
                        .multilineTextAlignment(.leading)
                        .font(.headline)
                }
                
                Spacer()
            })
            .padding()
            
        }
        
        .onAppear {
            if stateObject.detail == nil {
                stateObject.startLoad()
            }
        }
        
    }
    
    @StateObject var stateObject: DetailNewsObservableViewModel
    
    init(factory: FactoryType, identifier: Int, defaultNews: News?) {
        _stateObject = StateObject(wrappedValue: factory.detailNewsObservableViewModel(identifier: identifier, defaultNews: defaultNews))
    }
}

struct DetailNewsView_Previews: PreviewProvider {
    static var previews: some View {
        DetailNewsView(factory: Factory()!, identifier: 0, defaultNews: nil)
    }
}
