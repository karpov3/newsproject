//
//  RootCoordinator.swift
//  News
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation
import UIKit
import NewsViewModel
import NewsService

class UIKitNewsCoordinator: NavigationCoordinatorType, ParentChildCoordinatorType {

    // MARK: CoordinatorType
    typealias DataSourceType = Void

    func start(withDataSource dataSource: Void) {
        let vc = _viewController()
        if let viewController = vc as? ListNewsUIKitViewController {
            viewController.deallocating = { [weak self] in
                self?.removeFromParentCoordinator()
            }
        }
        navigationController?.pushViewController(vc, animated: true)
    }

    // MARK: ParentChildCoordinatorType
    var childCoordinators: [ParentChildCoordinatorType] = []
    var parentCoordinator: ParentChildCoordinatorType?

    var navigationController: UINavigationController?

    var factory: FactoryType

    init(navigationController: UINavigationController?, factory: FactoryType) {
        self.navigationController = navigationController
        self.factory = factory
    }

    private func _viewController() -> UIViewController {

        let viewModel = factory.listNewsViewModel(didSelect: {[weak self] news in

            guard let `self` = self else { return }

            let viewController = self._viewControllerDetail(news: news)
            self.navigationController?.pushViewController(viewController, animated: true)
        })
        return ListNewsUIKitViewController(viewModel: viewModel)
    }

    private func _viewControllerDetail(news: News) -> UIViewController {

        let viewModel = factory.detailNewsViewModel(identifier: news.id, defaultNews: news)

        return DetailNewsUIKitViewController(viewModel: viewModel)
    }

}
