//
//  RootCoordinator.swift
//  News
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation
import UIKit

class RootCoordinator: NavigationCoordinatorType, ParentChildCoordinatorType {

    // MARK: CoordinatorType
    typealias DataSourceType = Void

    func start(withDataSource dataSource: Void) {
        let collectionSetVC = _viewController()
        navigationController?.pushViewController(collectionSetVC, animated: true)
    }

    // MARK: ParentChildCoordinatorType
    var childCoordinators: [ParentChildCoordinatorType] = []
    var parentCoordinator: ParentChildCoordinatorType?

    var navigationController: UINavigationController?

    var factory: FactoryType

    init(navigationController: UINavigationController?, factory: FactoryType) {
        self.navigationController = navigationController
        self.factory = factory
    }

    private func _viewController() -> UIViewController {

        let viewModel = HomeViewModel(didSelectAction: {[weak self] kind in

            guard let `self` = self else { return }

            switch kind {

            case .uikit:

                let coordinator = UIKitNewsCoordinator(navigationController: self.navigationController, factory: self.factory)
                self.add(childCoordinator: coordinator)
                coordinator.start(withDataSource: ())

            case .swiftui:

                let coordinator = SwiftUINewsCoordinator(navigationController: self.navigationController, factory: self.factory)
                self.add(childCoordinator: coordinator)
                coordinator.start(withDataSource: ())
            }

        })
        return HomeViewController(viewModel: viewModel)
    }

}
