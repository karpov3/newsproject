//
//  RootCoordinator.swift
//  News
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation
import UIKit
import NewsViewModel
import NewsService
import SwiftUI

internal class ListNewsViewHostingcontroller: UIHostingController<ListNewsView> {

    var deallocating: (() -> Void)?

    deinit {
        if let closure = deallocating {
            closure()
        }
    }

}

class SwiftUINewsCoordinator: NavigationCoordinatorType, ParentChildCoordinatorType {

    // MARK: CoordinatorType
    typealias DataSourceType = Void

    func start(withDataSource dataSource: Void) {
        let vc = _viewController()

        navigationController?.pushViewController(vc, animated: true)
    }

    // MARK: ParentChildCoordinatorType
    var childCoordinators: [ParentChildCoordinatorType] = []
    var parentCoordinator: ParentChildCoordinatorType?

    var navigationController: UINavigationController?

    var factory: FactoryType

    init(navigationController: UINavigationController?, factory: FactoryType) {
        self.navigationController = navigationController
        self.factory = factory
    }

    private func _viewController() -> UIViewController {

        let viewController = ListNewsViewHostingcontroller(rootView: ListNewsView(factory: factory))
        viewController.deallocating = {[weak self] () in
            self?.removeFromParentCoordinator()
        }

        return viewController
    }

}
