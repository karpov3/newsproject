//
//  ListNewsUIKitViewController.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import SwiftUI
import NewsService
import NewsViewModel

struct ListNewsView: View {
    
    @State private var showingAlert = false
    
    var body: some View {
        
        List {
            ForEach(stateObject.news, id: \.self.id) { news in
                
                NavigationLink(destination: {
                    DetailNewsView(factory: factory, identifier: news.id, defaultNews: news)
                }, label: {
                    VStack(alignment: .leading, spacing: 8, content: {
                        Text("#\(news.id.description). \(news.title)")
                            .font(.title)
                        Text(news.subtitle)
                            .font(.headline)
                        Text(DateFormatter.label.string(from: news.timestamp))
                            .multilineTextAlignment(.leading)
                            .font(.footnote)
                        Spacer()
                    })
                })
                .onAppear {
                    
                    if news.id == stateObject.news.last?.id {
                        stateObject.startLoad()
                    }
                }
            }
            
            HStack {
                Text("Loading...")
                Spacer()
                ProgressView()
            }
            
        }
        .onReceive(stateObject.$errorMessage, perform: { message in
            if message != nil {
                showingAlert = true
            }
        })
        
        .alert(stateObject.errorMessage ?? "", isPresented: $showingAlert) {
            Button("OK", role: .cancel) {
                showingAlert = false
                stateObject.errorMessage = nil
            }
        }
        .listStyle(.plain)
        .navigationBarTitleDisplayMode(.inline)
        .onAppear {
            if stateObject.news.count == 0 {
                stateObject.startLoad()
            }
        }
        
    }
    
    // MARK: Initialization
    @StateObject var stateObject: ListNewsObservableViewModel
    
    var factory: FactoryType
    
    init(factory: FactoryType) {
        _stateObject = StateObject(wrappedValue: factory.listNewsObservableViewModel())
        self.factory = factory
    }
}

struct ListNewsView_Previews: PreviewProvider {
    static var previews: some View {
        ListNewsView(factory: Factory()!)
    }
}
