//
//  ListNewsObservableViewModel.swift
//  News
//
//  Created by Alexander Karpov on 23/09/22.
//

import Foundation
import NewsService
import NewsViewModel

class ListNewsObservableViewModel: NewsListViewModel, ObservableObject {

    @Published private(set) var news: [News] = []
    @Published private(set) var loadingChunks: Int = 0
    @Published var errorMessage: String?

    @MainActor func startLoad() {
        self.loadingChunks += 1

        self.load {[weak self] result in

            guard let `self` = self else { return }

            do {
                self.news = try result.get()
            } catch {
                self.errorMessage = error.localizedDescription
                if let error = error as? NewsError {
                    self.errorMessage = error.description
                }            }
            self.loadingChunks -= 1
        }

    }

    public init(service: NewsServiceType) {
        super.init(service: service, didSelect: { _ in
        })

    }
}
