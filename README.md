
# Project News Delivery
 
The project was created with the MVVM + Coordinator pattern for the Navigation part and the UI. Instead for the Model, Networking and Base ViewModel part the *modular application* concept was used and then the logics were moved to the local SPM packages (NewsService and NewsViewModel). Also added a Test example for the Detail and ListNews models.

The credentials were placed in the **configuration.json** .

# Progetto News
 
The project consists in the creation of a simple app for consulting news from the web.

## Delivery Requirements
 
The language to use is **Swift**.
 
- retrieve the **news** list from an API authenticated with ** api key **.
- show the news list on the screen.
- give the possibility to select a news and bring to a detail page.
- retrieve and show the **news** detail from a second API authenticated in the same way as the previous one.
- don't use RxSwift or Combine
 
## Interface
 
- The interface will be developed with both **SwiftUI** and **UIKit**.
- Provide a selector on the Homepage to choose the framework used to develop the interface (as shown in the figure).
 
![figura 1](image.png)
 
 
### News List
 
The news list is retrievable with a `GET` at the` / news` path.
 
This API provides for the use of 2 parameters in query strings to manage the pagination of the returned data.
 
- limit: number
- offset: number
 
The expected payload is in the form:
 
```json
{
    "message": {
        "results": [
            {
                "id": 0,        // integer
                "title": "",    // string
                "subtitle": "", // string
                "timestamp": 0  // timestamp in ms
            },
            ...
        ],
        "size": 0,              // number
        "parameters": {
            "limit": 0,         //number
            "offset": 0,        //number
        }
    }
}
```
 
NB: This API has been deliberately slowed down in proportion to the number of data requested.
 
 
### News detail
 
The news detail can be retrieved with a `GET` at the path` / news / {id} `.
 
The expected payload is in the form:
 
```json
{
    "message": {
        "id": 0,        // integer
        "title": "",    // string
        "subtitle": "", // string
        "body": "",     // string
        "timestamp": 0  // timestamp in ms
    }
}
```
 
