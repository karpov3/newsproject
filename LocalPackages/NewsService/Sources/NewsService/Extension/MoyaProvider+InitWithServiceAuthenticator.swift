//
//  File.swift
//  
//
//  Created by Alexander Karpov on 24/09/22.
//

import Foundation
import Moya

public extension MoyaProvider {

    convenience init(endpointClosure: @escaping EndpointClosure = MoyaProvider.defaultEndpointMapping,
                     requestClosure: @escaping RequestClosure = MoyaProvider.defaultRequestMapping,
                     stubClosure: @escaping StubClosure = MoyaProvider.neverStub,
                     manager: Session = MoyaProvider<Target>.defaultAlamofireSession(),
                     serviceAuthenticator: RequestInterceptor?,
                     plugins: [PluginType] = [],
                     trackInflights: Bool = false) {

        var session = manager
        if let authenticator = serviceAuthenticator {
            let configuration = URLSessionConfiguration.default
            session = Session(configuration: configuration, interceptor: authenticator)
        }

        self.init(endpointClosure: endpointClosure,
                  requestClosure: requestClosure,
                  stubClosure: stubClosure,
                  session: session,
                  plugins: plugins,
                  trackInflights: trackInflights)
    }
}
