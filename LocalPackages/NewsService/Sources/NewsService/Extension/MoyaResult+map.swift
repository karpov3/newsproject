//
//  File.swift
//  
//
//  Created by Alexander Karpov on 24/09/22.
//

import Foundation
import Moya
extension Swift.Result where Success == Moya.Response, Failure == MoyaError {

    public func map<T: Decodable>(type: T.Type, decoder: JSONDecoder) -> Result<T, NewsError> {

        switch self {
        case .success(let response):
            do {
                return .success(try decoder.decode(T.self, from: response.data))
            } catch {
                print(error)
                return .failure(.badResponse)
            }

        case .failure(let failure):
            switch failure {
            case .statusCode(let response) where response.statusCode == 404:
                return .failure(.notFound)
            default:
                return .failure(.unknown)
            }

        }

    }
}
