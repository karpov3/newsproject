//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation

public protocol NewsServiceType {

    func list(limit: Int, offset: Int, completion: @escaping (Result<Response<ListNews>, NewsError>) -> Void)

    func detail(_ identifier: Int, completion: @escaping (Result<Response<Detail>, NewsError>) -> Void)

}
