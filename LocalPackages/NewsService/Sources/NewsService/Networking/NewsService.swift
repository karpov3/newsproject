//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation
import Moya

public enum NewsServiceRequest {

    case list([String: Any])

    case detail(Int)
}

public struct NewsService {

    public let serviceRequest: NewsServiceRequest

    public let baseURL: URL

    init(serviceRequest: NewsServiceRequest, baseURL: URL) {
        self.serviceRequest = serviceRequest
        self.baseURL = baseURL
    }
}

extension NewsService: Moya.TargetType {

    public var path: String {
        switch serviceRequest {
        case .list:
            return "/news"
        case .detail(let identifier):
            return "/news/\(identifier)"
        }
    }

    public var method: Moya.Method {
        switch serviceRequest {
        case .detail, .list:
            return .get
        }
    }

    public var sampleData: Data { Data() }

    public var task: Task {
        switch serviceRequest {
        case .list(let query):
            return .requestParameters(parameters: query, encoding: URLEncoding.default)
        case .detail:
            return .requestPlain
        }
    }

    public var headers: [String: String]? {
        return nil
    }

    public var validationType: ValidationType {
        return .successCodes
    }

}
