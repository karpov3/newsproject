//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation

public struct ServiceConfiguration {

    public let baseURL: URL

    public let apiKey: String

}

extension ServiceConfiguration: Codable {}
