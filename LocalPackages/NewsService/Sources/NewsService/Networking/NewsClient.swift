//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation
import Moya

public class NewsClient {

    let provider: MoyaProvider<NewsService>

    let baseURL: URL

    public init(baseURL: URL, provider: MoyaProvider<NewsService>) {

        self.baseURL = baseURL
        self.provider = provider

    }
}

extension NewsClient: NewsServiceType {

    public func list(limit: Int, offset: Int, completion: @escaping (Result<Response<ListNews>, NewsError>) -> Void) {

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .millisecondsSince1970

        provider.request(NewsService(serviceRequest: .list(["limit": limit, "offset": offset]), baseURL: baseURL)) { completion($0.map(type: Response<ListNews>.self, decoder: decoder))
        }

    }

    public func detail(_ identifier: Int, completion: @escaping (Result<Response<Detail>, NewsError>) -> Void) {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .millisecondsSince1970

        provider.request(NewsService(serviceRequest: .detail(identifier), baseURL: baseURL)) {
            completion($0.map(type: Response<Detail>.self, decoder: decoder))
        }
    }

}
