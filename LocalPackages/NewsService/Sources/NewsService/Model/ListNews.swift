//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation

public struct ListNews {

    public struct QueryParameter {

        public let limit: Int

        public let offset: Int

        enum CodingKeys: String, CodingKey {
            case limit, offset
        }
    }

    public let size: Int

    public let parameters: QueryParameter

    public let results: [News]

    enum CodingKeys: String, CodingKey {
        case size, parameters, results
    }

}

// MARK: Extensions

extension ListNews: Decodable {

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let size = try values.decode(Int.self, forKey: .size)
        let parameters = try values.decode(QueryParameter.self, forKey: .parameters)

        self.size = size
        self.parameters = parameters

        let throwables = try values.decode([Throwable<News>].self, forKey: .results)
        results = throwables.compactMap { try? $0.result.get() }
    }
}

extension ListNews.QueryParameter: Codable {

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let limit = try values.decode(String.self, forKey: .limit)
        let offset = try values.decode(String.self, forKey: .offset)

        guard let limit = Int(limit), let offset = Int(offset) else {
            throw DecodingError.dataCorrupted(.init(codingPath: [CodingKeys.limit, CodingKeys.offset], debugDescription: "`offset` and `limit` must be convertible to an integer"))
        }

        self.limit = limit
        self.offset = offset

    }
}

public struct Throwable<T: Decodable>: Decodable {

    public let result: Result<T, Error>

    public init(from decoder: Decoder) throws {
        let catching = { try T(from: decoder) }
        result = Result(catching: catching )
    }
}
