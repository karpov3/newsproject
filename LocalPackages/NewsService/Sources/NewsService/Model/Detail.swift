//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation

public struct Detail {

    public let news: News

    public let body: String

    internal enum CodingKeys: String, CodingKey {
        case news, body
    }
}

// MARK: Extensions

extension Detail: Decodable {

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        body = try values.decode(String.self, forKey: .body)

        let container = try decoder.singleValueContainer()
        news = try container.decode(News.self)
    }
}
