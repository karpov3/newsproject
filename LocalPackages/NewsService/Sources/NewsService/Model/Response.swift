//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation

public struct Response<T: Decodable> {

    public let message: T

}

// MARK: Extensions

extension Response: Decodable {}
