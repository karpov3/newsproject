//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation

public struct News {

    public let id: Int

    public let title: String

    public let subtitle: String

    public let timestamp: Date
}

// MARK: Extensions

extension News: Decodable {}

// MARK: Extensions

extension News: Identifiable {}
