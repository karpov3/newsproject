//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation
import Alamofire

public class ServiceAuthenticator: RequestInterceptor {

    public let apiKey: String

    public init(apiKey: String) {
        self.apiKey = apiKey
    }

    public func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        var newUrlRequest = urlRequest
        newUrlRequest.addValue(apiKey, forHTTPHeaderField: "x-api-key")
        completion(.success(newUrlRequest))
    }
}
