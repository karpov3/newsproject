//
//  NewsError.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation

public enum NewsError: Error {

    case notFound, badResponse, unknown

}

extension NewsError: CustomStringConvertible {

    public var description: String {
        switch self {

        case .notFound:
            return "News not found"
        case .badResponse:
            return "There are some problems with services. Please retry."
        case .unknown:
            return "Some error occurred"
        }
    }
}
