//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation
import NewsService

open class NewsListViewModel: ListNewsViewModeling {

    internal let limit = 50
    internal var offset = 0
    var news: [News] = []

    @MainActor public func load(completion: @escaping (Result<[News], NewsError>) -> Void) {

        if news.count == 0 {
            offset = 0
        }

        let newOffset = offset

        // Update pagination
        offset += limit

        service.list(limit: limit, offset: newOffset) {[weak self] result in

            guard let `self` = self else { return }

                switch result {
                case .success(let list):
                    self.news += list.message.results
                    completion(.success(self.news))
                case .failure(let error):
                    self.news = []
                    completion(.failure(error))
                }
        }
    }

    public func didSelect(news: News) {
        self.didSelect(news)
    }

    // MARK: Initialization

    let service: NewsServiceType

    let didSelect: (News) -> Void

    public init(service: NewsServiceType, didSelect: @escaping (News) -> Void) {
        self.service = service
        self.didSelect = didSelect
    }

}
