//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation
import NewsService

public protocol ListNewsViewModeling {

    func didSelect(news: News)

    @MainActor func load(completion: @escaping (Result<[News], NewsError>) -> Void)

}
