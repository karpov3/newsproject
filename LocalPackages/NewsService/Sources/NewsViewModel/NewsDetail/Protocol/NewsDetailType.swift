//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation
import NewsService

public protocol DetailNewsViewModeling {

    func load(completion: @MainActor @escaping (Result<Detail, NewsError>) -> Void)

    var defaultNews: News? { get }

}
