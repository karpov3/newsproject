//
//  File.swift
//  
//
//  Created by Alexander Karpov on 22/09/22.
//

import Foundation
import NewsService

open class NewsDetailViewModel: DetailNewsViewModeling {

    // MARK: DetailNewsViewModeling

    public func load(completion: @escaping @MainActor (Result<Detail, NewsError>) -> Void) {

        service.detail(identifier) {result in
            Task {
                await completion(result.map {$0.message})
            }
        }

    }

    // MARK: Initialization

    let service: NewsServiceType

    public let defaultNews: News?

    let identifier: Int

    public init(service: NewsServiceType, identifier: Int, defaultNews: News?) {
        self.service = service
        self.identifier = identifier
        self.defaultNews = defaultNews
    }

}
