// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "NewsService",
    defaultLocalization: "en",
    platforms: [ .iOS(.v15), .macOS(.v11) ],
    products: [

        .library(
            name: "NewsService",
            targets: ["NewsService"]),

            .library(
                name: "NewsViewModel",
                targets: ["NewsViewModel"])
    ],
    dependencies: [
        .package(url: "https://github.com/Alamofire/Alamofire.git", .upToNextMajor(from: "5.6.2")),
        .package(url: "https://github.com/Moya/Moya.git", .upToNextMajor(from: "15.0.3"))
    ],
    targets: [
        .target(
            name: "NewsService",
            dependencies: [
                .product(name: "Moya", package: "Moya")
            ]),

            .testTarget(
                name: "NewsServiceTests",
                dependencies: ["NewsService"],
                resources: [
                    .process("Resources")
                ]),

            .target(
                name: "NewsViewModel",
                dependencies: [
                    "NewsService"
                ]),

            .testTarget(
                name: "NewsViewModelTests",
                dependencies: [
                    "NewsViewModel"
                ])
    ]
)
