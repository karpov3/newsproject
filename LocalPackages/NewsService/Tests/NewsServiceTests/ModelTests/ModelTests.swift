import XCTest
@testable import NewsService

final class NewsServiceTests: XCTestCase {
    func testListNewsDecodable() throws {

        guard
            let url = Bundle.module.url(forResource: "listNews", withExtension: "json") else {
            print("Resource 'listNews.json' not found")
            return
        }

        let data = try Data(contentsOf: url)

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .millisecondsSince1970

        
        let listNews = try decoder.decode(Response<ListNews>.self, from: data)

        XCTAssertEqual(listNews.message.size, 3)
        XCTAssertEqual(listNews.message.parameters.limit, 3)
        XCTAssertEqual(listNews.message.parameters.offset, 3)
        XCTAssertEqual(listNews.message.results.first?.id, 3)
        XCTAssertEqual(listNews.message.results.first?.title, "Quisque maximus felis vel felis mollis varius")
        XCTAssertEqual(listNews.message.results.first?.subtitle, " Mauris egestas gravida dolor sed interdum")
        XCTAssertEqual(listNews.message.results.first?.timestamp, Date(timeIntervalSince1970: TimeInterval(1624184299409) / 1000))
        
        // Count == 2 because one item has bad structure
        XCTAssertEqual(listNews.message.results.count, 2)

    }

    func testDetailNewsDecodable() throws {

        guard
            let url = Bundle.module.url(forResource: "detailNews", withExtension: "json") else {
            print("Resource 'listNews.json' not found")
            return
        }

        let data = try Data(contentsOf: url)

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .millisecondsSince1970

        let detail = try decoder.decode(Response<Detail>.self, from: data)

        XCTAssertEqual(detail.message.news.id, 3)
        XCTAssertEqual(detail.message.news.title, "Quisque maximus felis vel felis mollis varius")
        XCTAssertEqual(detail.message.news.subtitle, " Mauris egestas gravida dolor sed interdum")
        XCTAssertEqual(detail.message.news.timestamp, Date(timeIntervalSince1970: TimeInterval(1624184299409) / 1000))
        XCTAssertEqual(detail.message.body, "Quisque maximus felis vel felis mollis varius. Mauris egestas gravida dolor sed interdum. Duis pharetra scelerisque ante, facilisis pulvinar tellus rhoncus in. Integer ac libero tempus, laoreet mi quis, blandit tortor. Morbi ut nunc fermentum, euismod dui id, ullamcorper nibh. Maecenas convallis eleifend aliquam. Suspendisse potenti. Pellentesque eu gravida justo. Nulla bibendum ipsum vel dolor euismod elementum. Quisque congue, neque vel mattis posuere, velit erat euismod odio, consectetur ultricies lacus tortor at elit. Quisque venenatis rutrum eros, in facilisis arcu euismod id. Pellentesque accumsan orci id leo mattis euismod eu at ex. Nulla ut arcu ligula. Aliquam erat volutpat. Fusce sed mauris fringilla, pharetra purus interdum, volutpat mi.")

    }
}
