import XCTest
import Moya
@testable import NewsService

final class NetworkingTests: XCTestCase {

    func testSampleRequest() async throws {

        guard
            let url = Bundle.module.url(forResource: "configuration", withExtension: "json") else {
            print("Resource 'configuration.json' not found")
            return
        }

        let data = try Data(contentsOf: url)

        let config = try JSONDecoder().decode(ServiceConfiguration.self, from: data)

        let authenticator = ServiceAuthenticator(apiKey: config.apiKey)
        let provider = MoyaProvider<NewsService>(serviceAuthenticator: authenticator)
        let service: NewsServiceType = NewsClient(baseURL: config.baseURL, provider: provider)

        service.list(limit: 2, offset: 3) { result in

            switch result {

            case .success:
                XCTAssertTrue(true)

            case .failure(let err):
                XCTAssertEqual(err, NewsError.unknown)

            }
        }
    }

}
